import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaComponent } from './sala/sala.component';


const routes: Routes = [
  {path: 'index', component: SalaComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'index'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }