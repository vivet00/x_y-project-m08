import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-sala',
  templateUrl: './sala.component.html',
  styleUrls: ['./sala.component.css']
})
export class SalaComponent {

  it: Observable<any[]>;
  constructor(db: AngularFirestore) {
    this.it = db.collection('items').valueChanges();   
    
  }
  // Array with the the points of all players in every iteration
  Punts : number[][] = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
  // Array with the total points of every player
  PuntsTotal : number[] = [0,0,0,0]

 // This function will be called every time a value in firebase is changed
  calculate(a: string[], j: number) {
    
    let x = 0;
    let y = 0;
    // This loop counts how many X and Y are in the current iteration
    for (let i = 0; i < a.length; i++){
      if (a[i] == "X")
        x++;
      else
        y++;
    }

    if (y == 4){
      // All the players +1
      for (let i = 0; i < this.Punts[0].length; i++){
        this.Punts[j][i] = 1
      }
    }      
    else if (y == 3){
      // Players who voted Y lose 1, player who voted X win 3
      for (let i = 0; i < this.Punts[0].length; i++){
        if (a[i] == "X")
          this.Punts[j][i] = 3
        else
        this.Punts[j][i] = -1
      }
    }      
    else if (y == 2){
      // Players who voted Y lose 2, players who voted X win 2
      for (let i = 0; i < this.Punts[0].length; i++){
        if (a[i] == "X")
          this.Punts[j][i] = 2
        else
        this.Punts[j][i] = -2
      }
    }
    else if (y == 1){
      // Player who voted Y lose 3, players who voted X win 3
      for (let i = 0; i < this.Punts[0].length; i++){
        if (a[i] == "X")
          this.Punts[j][i] = 1
        else
          this.Punts[j][i] = -3
      }
    }
    else{
      // Everyone loses 1
      for (let i = 0; i < this.Punts[0].length; i++){
        this.Punts[j][i] = -1
      }
    }
    // After all the current iteration points have been calculated, the total points array is updated.
    for (let i = 0; i < this.PuntsTotal.length; i++){
      this.PuntsTotal[i] = this.Punts[0][i] + this.Punts[1][i] + this.Punts[2][i] + this.Punts[2][i]
    }

  }  

  // This function checks if all the values are valid in the current iteration,
  // it's used for the conditional rendering of the rows.
  isNotEmty(a: string[]){
    return (a[0] == "X" || a[0] == "Y") && (a[1] == "X" || a[1] == "Y") && (a[2] == "X" || a[2] == "Y") && (a[3] == "X" || a[3] == "Y")
  }


  

}

