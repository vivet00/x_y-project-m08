// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBSuQbFzt4AFRsBBG4SD4lj6vui8SO4XuE",
    authDomain: "project-xy-6a76e.firebaseapp.com",
    databaseURL: "https://project-xy-6a76e.firebaseio.com",
    projectId: "project-xy-6a76e",
    storageBucket: "project-xy-6a76e.appspot.com",
    messagingSenderId: "152680010247",
    appId: "1:152680010247:web:04407e7997eb3960bbd0a2",
    measurementId: "G-23965XQB1W"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
